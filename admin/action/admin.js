$(document).ready(function() {
  $('.nav-link').css('color','#001737');
  $('#nav-master').css('color','#0168fa');
  get_data();
});

function get_data(){
  $('#main-table > tbody').empty();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Admin/get_data/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      var aaData = [];
      $.each(response.data, function( i, value ) {
        var stringData  = value._;
        var no          = i + 1;

        aaData.push([
          `<div style="text-align:center">` + no + `</div>`,
          value.nama_admin,
          value.username_admin,
          value.jabatan,
          value.email_admin,
          value.wa_admin,
          `<div style="text-align:center; color:#0168fa">
            <a onclick="edit_data(`+value.id+`,'`+value.nama_admin+`','`+value.username_admin+`','`+value.jabatan+`','`+value.email_admin+`','`+value.wa_admin+`')" class="tx-12 tx-medium" data-toggle="tooltip" title="Edit" style="cursor:pointer"><i class="fas fa-edit"></i></a>
            <font class="tx-12 tx-info">|</font>
            <a class="tx-12 tx-medium" onclick="delete_data(`+value.id+`)" data-toggle="tooltip" title="Delete" style="cursor:pointer"><i class="fas fa-trash"></i></a>
          </div>`,
        ]);
      });
      $('#main-table').DataTable().destroy();
      $('#main-table').DataTable({
      // 'responsive': true,
        'language'  : {
          'searchPlaceholder'   : 'Search...',
          'sSearch'       : '',
          'lengthMenu'      : '_MENU_ items/page',
          },
        'aaData'  : aaData,
        "bJQueryUI" : true,
        "aoColumns" : [
          { "sWidth"  : "1%" },  
          { "sWidth"  : "15%" }, 
          { "sWidth"  : "15%" }, 
          { "sWidth"  : "15%" }, 
          { "sWidth"  : "15%" }, 
          { "sWidth"  : "15%" }, 
          { "sWidth"  : "5%" }, 
        ],
        "aLengthMenu": [[10, 20, 50, 100],[ 10, 20, 50, 100]],
      });
    },
  });
}

function requiredField(val,field){
  var value   = $.trim(val);
  if (field == 'wa_admin') {
    if (value.length == 1 && value == '0') { $('#wa_admin').val('62');}
    if (value.length > 9) { valid(field);} else { origin(field);}
  } else if (field == 'username_admin'){
    if(value.length > 3){
      $.ajax({
        type: 'POST',
        dataType: 'json',
        cache: false,
        url: configUrl + 'Admin/check/_',
        data: {
          check : value,
          type  : 'username_admin',
        },
        success: function (responses) {
          if(responses.code == 200 && responses.total == 0){
            valid(field);
          }else {
            if ($('#act').val() == 'add') {
              invalid(field);
            } else {
              if (responses.data.username_admin == $('#username_adminCopy').val()) {
                valid(field);
              } else {
                invalid(field);
              }
            }
          }
        }
      }); 
    } else {
      origin(field);
    }
  } 
}

function valid(field){
  $('#is'+field).val(1);
  $('#'+field).css('border','1px solid #c0ccda');
}

function invalid(field){
  $('#is'+field).val(0); 
  $('#'+field).css('border','1px solid red');
}

function origin(field){
  $('#is'+field).val(0); 
  $('#'+field).css('border','1px solid #c0ccda');
}

function add_data(){
  $('#act').val('add');
  $('#id').val('0');
  $('#nama_admin').val('');
  $('#username_admin').val('');
  $('#username_adminCopy').val('');
  $('#isusername_admin').val(0);
  $('#username_admin').css('border','1px solid #c0ccda');
  $('#jabatan').val('');
  $('#email_admin').val('');
  $('#wa_admin').val('');
  $('#iswa_admin').val(0);
  $('#wa_admin').css('border','1px solid #c0ccda');
  $('#add').css('display','flex');
  $('#edit').css('display','none');
  $('#modal_data').modal('show');
}

function edit_data(id, nama_admin, username_admin, jabatan, email_admin, wa_admin){
  $('#act').val('edit');
  $('#id').val(id);
  $('#nama_admin').val(nama_admin);
  $('#username_admin').val(username_admin);
  $('#username_adminCopy').val(username_admin);
  $('#isusername_admin').val(1);
  $('#username_admin').css('border','1px solid #c0ccda');
  $('#jabatan').val(jabatan);
  $('#email_admin').val(email_admin);
  $('#wa_admin').val(wa_admin);
  $('#iswa_admin').val(1);
  $('#wa_admin').css('border','1px solid #c0ccda');
  $('#add').css('display','none');
  $('#edit').css('display','block');
  $('#resetPassword').prop('checked',false);
  $('#modal_data').modal('show');
}

function save(){
  if ($('#nama_admin').val() == '') {
    alert('Form harus dilengkapi');
  } else if ($('#isusername_admin').val() == 0){
    alert('Username tidak tersedia');
  } else if ($('#iswa_admin').val() == 0){
    alert('Nomor whatsapp tidak valid');
  } else {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Admin/save/_',
      data: $('#form_master').serialize(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(responses) {
        if (responses.code == 200) {
          get_data();
          $('#modal_data').modal('hide');
        } else {
          alert('failed');
        }
      }, 
    });
  }
}

function delete_data(id){
  if(confirm("Anda yakin mengapus admin ini?")){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Admin/delete/_',
      data: {
        id : id
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(responses) {
        if (responses.code == 200) {
          get_data();
        } else {
          alert('failed');
        }
      }, 
    });
  }
}