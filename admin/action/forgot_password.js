var configUrl   = 'http://asitajatim.org/server/Admin/';

$(document).ready(function() {
});

function forgotPassword(){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Login/forgot_password/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            username	: $("#username").val(),
        },
        success: function(response) {
            if (response.code == '200') {
                if (response.data.email_admin == '') {
                    alert('Email anda tidak terdaftar pada akun ini');
                } else if(response.data.wa_admin == '') {
                    alert('Whatsapp anda tidak terdaftar pada akun ini');
                } else {
                    var array   = response.data.email_admin.split('@').join(',').split('.').join(',');
                    var arr     = array.split(",");

                    var name1   = arr[0].charAt(0);
                    var name2   = arr[0].charAt(arr[0].length - 1);
                    var sub1    = arr[1].charAt(0);
                    var sub2    = arr[1].charAt(arr[1].length - 1);

                    var noWA    = response.data.wa_admin.substr(response.data.wa_admin.length-4, 4);

                    $('#field1').css('display','none');
                    $('#field2').css('display','block');
                    $('#reset_agentName').html(response.data.nama_admin);
                    $('#reset_whatsappNo').html(`****-****-`+noWA);
                    $('#reset_email').html(name1+`*****`+name2+`@`+sub1+`*****`+sub2+`.`+arr[2]);

                    send_authCode(response.data.wa_admin,response.auth);

                    $('#changePassword').click(function() {
                        var authCode = atob(response.auth);
                        var veriCode = $('#auth').val();

                        if (authCode == veriCode) {
                            changePassword(response.data.id, response.data.username_admin);
                        } else {
                            alert('Your authentication code is not verified.');
                        }
                    });


                    $('#send_authCode').click(function() {
                        send_authCode(response.data.wa_admin,response.auth);
                    });
                }
            }else{
                alert("Your data is not verified");
            }
        },
    });
}

function send_authCode(wa, code){
    var whatsapp = wa.replace(/[-+]/g,'');
    if(whatsapp.slice(0,1) == '0'){
        var phone = '62'+whatsapp.substring(1);
    }else if(whatsapp.slice(0,2) == '62'){
        var phone = whatsapp;
    }

    $.ajax({
        type: 'POST',
        url: 'https://tghapisystem.net/wablash/module/single_message',
        dataType: 'json',
        data: {
            'phone'   : phone,
            'message' : `Your ASITA ADMIN authentication code : `+atob(code),
        },
        success: function(responses) {
            if(responses.status==true){
            }else{
                alert(responses.message);
            }
        }
    });   
}

function changePassword(id,username){
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Login/reset_password/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            id			: id,
            password    : '123456',
        },
        success: function(responses) {
            if (responses.data == true) {
            	alert('Your account has been successfully reset');
            	window.location.href = '../pages/login';
            }else{
                alert("Your data is not verified");
            }
        },
    });
}