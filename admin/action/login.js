var url = getUrlParameter('activate');
var configUrl   = 'http://asitajatim.org/server/Admin/';

function getUrlParameter(sParam){
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
  }
}

$('input[name="username"]').focus();

$(document).ready(function() { 
    if (url != null) {
        setTimeout(function () {
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: configUrl + 'Login/activate/_',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: {
                    token   : '1b787b70ed2e0de697d731f14b5da57b',
                    id      : url,
                },
                success: function(response) {
                    if (response.code == '200') {
                        $('#txt_alert').css('display','none');
                        $('#txt_success').css('display','block');
                        $('#txt_success').html('<b>Success</b><br>Your account has successfully activated, get started!');
                    }else{
                        $('#txt_success').css('display','none');
                        $('#txt_alert').css('display','block');
                        $('#txt_alert').html('<b>Failed</b><br>Your account failed to activate, try again later');
                    }
                },
            });
        }, 1000);
    }

    if (localStorage.getItem(btoa('asita_id')) != null && localStorage.getItem(btoa('asita_name')) != null && localStorage.getItem(btoa('asita_username')) != null && localStorage.getItem(btoa('asita_token')) != null && localStorage.getItem(btoa('asita_expired'))) {
        window.location.href = '../pages/dashboard';
    }
    
    $('#password').keypress(function(e) {
        if(e.which == 13) {
            login($("#username").val(),$("#password").val());
        }
    });

    $('#login').click(function() {
        login($("#username").val(),$("#password").val());
    });
});

function login(username, password){  
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Login/login/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            token       : '1b787b70ed2e0de697d731f14b5da57b',
            username    : username,
            password    : password,
        },
        success: function(response) {
            if (response.code == '200') {
                localStorage.setItem(btoa('asita_id'), btoa(response.data.id));
                localStorage.setItem(btoa('asita_name'), btoa(response.data.nama_admin));
                localStorage.setItem(btoa('asita_username'), btoa(response.data.username_admin));
                localStorage.setItem(btoa('asita_token'), btoa(response.data.token));
                localStorage.setItem(btoa('asita_expired'), btoa(response.data.expired));
                window.location.href = '../pages/dashboard';
            }else{
                $('#btn_signin').html('Sign In');
                $('#txt_success').css('display','none');
                $('#txt_alert').css('display','block');
                $('#txt_alert').html('<b>'+response.message+'</b><br>'+response.errors.message);
            }
        },
    });
}