var url     = atob(getUrlParameter('search'));

$(document).ready(function() {
  $('.nav-link').css('color','#001737');
  $('#nav-agent').css('color','#0168fa');

  setTimeout(function () {
    $("#id_status").val(url);
    get_data();
  }, 500);

  $("#id_status").change(function () {
    get_data();
  });

  masterCity();

  $("#fileThumbnail").change(function () {
    uploadImg('fileThumbnail','namecard');
  });
});

function get_data(){
  $('#main-table > tbody').empty();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Agent/get_data/_',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data : {
      id_status : $('#id_status').val(),
    },
    success: function(response) {
      var aaData = [];
      $.each(response.data, function( i, value ) {
        var stringData  = value._;
        var no          = i + 1;

        var approve; var button;
        if (value.id_status == 0) {
          approve = `<i class="fas fa-clock" style="color:purple"></i><i> menunggu review</i>`;
          button  = `<div style="text-align:center; color:#0168fa">
                      <a onclick="get_details(`+value.id+`)" class="tx-12 tx-medium" data-toggle="tooltip" title="Details" style="cursor:pointer"><i class="fas fa-search"></i></a>
                    </div>`;
        } else if (value.id_status == 1) {
          approve = `<i class="fas fa-check-circle" style="color:green" data-toggle="tooltip" title="`+value.nama_admin+`"></i> ` + dateTimeFormat(value.tgl_disetujui);  
          button  = `<div style="text-align:center; color:#0168fa">
                      <a onclick="get_details(`+value.id+`)" class="tx-12 tx-medium" data-toggle="tooltip" title="Details" style="cursor:pointer"><i class="fas fa-search"></i></a>
                      <font class="tx-12 tx-info mx-1">|</font>
                      <a onclick="edit(`+value.id+`)" class="tx-12 tx-medium" data-toggle="tooltip" title="Edit" style="cursor:pointer"><i class="fas fa-edit"></i></a>
                    </div>`;
        } else if (value.id_status == 2){
          approve = `<i class="fas fa-times-circle" style="color:red" data-toggle="tooltip" title="`+value.nama_admin+`"></i><i> ditolak</i>`;
          button  = `<div style="text-align:center; color:#0168fa">
                      <a onclick="get_details(`+value.id+`)" class="tx-12 tx-medium" data-toggle="tooltip" title="Details" style="cursor:pointer"><i class="fas fa-search"></i></a>
                    </div>`;  
        } else {
          approve = `<i class="fas fa-clock" style="color:orange"></i><i> belum verifikasi</i>`;
          button  = `<div style="text-align:center; color:#0168fa">
                      <a onclick="get_details(`+value.id+`)" class="tx-12 tx-medium" data-toggle="tooltip" title="Details" style="cursor:pointer"><i class="fas fa-search"></i></a>
                      <font class="tx-12 tx-info mx-1">|</font>
                      <a onclick="sendWA_verifikasi('`+value.gelar_pimpinan+`','`+value.nama_pimpinan+`','`+value.no_wa+`','`+value.nama_perusahaan+`','`+value.nomor_anggota+`','`+value.alamat+`','`+value.kodepos+`','`+value.telp+`','`+value.email+`','`+value.email_pimpinan+`','`+value.code+`')" class="tx-12 tx-medium" data-toggle="tooltip" title="Kirim ulang whatsapp" style="cursor:pointer;color:green"><i class="fab fa-whatsapp"></i></a>
                    </div>`;  
        }
        aaData.push([
          `<div style="text-align:center">` + no + `</div>`,
          value.nama_perusahaan.toUpperCase(),
          value.alamat,
          value.kota,
          value.nama_pimpinan,
          `62`+value.no_wa,
          `<div style="text-align:center">` + dateTimeFormat(value.tgl_registrasi) + `</div>`,
          `<div style="text-align:center">` + approve + `</div>`,
          button,
        ]);
      });
      $('#main-table').DataTable().destroy();
      $('#main-table').DataTable({
      // 'responsive': true,
        'language'  : {
          'searchPlaceholder'   : 'Search...',
          'sSearch'       : '',
          'lengthMenu'      : '_MENU_ items/page',
          },
        'aaData'  : aaData,
        "bJQueryUI" : true,
        "aoColumns" : [
          { "sWidth"  : "1%" },  
          { "sWidth"  : "15%" }, 
          { "sWidth"  : "15%" }, 
          { "sWidth"  : "15%" }, 
          { "sWidth"  : "12%" }, 
          { "sWidth"  : "10%" }, 
          { "sWidth"  : "12%" }, 
          { "sWidth"  : "12%" }, 
          { "sWidth"  : "10%" }, 
        ],
        "aLengthMenu": [[10, 20, 50, 100],[ 10, 20, 50, 100]],
      });
    },
  });
}

function get_details(id){
  $('#tb_data').empty();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Agent/get_details/_',
    data: {
      id : id
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.status == true) {
        var namecard = response.data.file_kartu_nama == '' ? '<small style="color:red">Tidak ada kartu nama</small>' : `<a href="http://asitajatim.org/files/namecard/`+ response.data.file_kartu_nama +`" target="_blank">lihat gambar</a>`;
        $('#tb_data').append(`
          <tr>
            <td>Nama Perusahaan</td>
            <th>`+ response.data.nama_perusahaan +`</th>
          </tr>
          <tr>
            <td>Nama PT</td>
            <th>`+ response.data.nama_pt +`</th>
          </tr>
          <tr>
            <td>Nomor Anggota</td>
            <th>`+ response.data.no_anggota +`</th>
          </tr>
          <tr>
            <td>Alamat</td>
            <th>`+ response.data.alamat +`</th>
          </tr>
          <tr>
            <td>Alamat 2</td>
            <th>`+ response.data.alamat_2 +`</th>
          </tr>
          <tr>
            <td>Provinsi</td>
            <th>`+ response.data.provinsi +`</th>
          </tr>
          <tr>
            <td>Kota</td>
            <th>`+ response.data.kota +`</th>
          </tr>
          <tr>
            <td>No Telp</td>
            <th>`+ response.data.telp +`</th>
          </tr>
          <tr>
            <td>Email</td>
            <th>`+ response.data.email +`</th>
          </tr>
          <tr>
            <td>Nama Pimpinan</td>
            <th>`+ response.data.gelar_pimpinan +`. `+ response.data.nama_pimpinan +`</th>
          </tr>
          <tr>
            <td>Email Pimpinan</td>
            <th>`+ response.data.email_pimpinan +`</th>
          </tr>
          <tr>
            <td>Nomor Whatsapp</td>
            <th>`+ response.data.no_wa +`</th>
          </tr>
          <tr>
            <td>Kartu Nama</td>
            <th>`+namecard+`</th>
          </tr>
        `);
        if (response.data.id_status == 0) {
          $('#modal_footer').html(`
            <button type="button" onclick="approve(`+response.data.id+`, 1, '`+response.data.nama_pimpinan+`', '`+response.data.no_wa+`')" class="btn btn-success tx-13">Terima</button>
            <button type="button" onclick="approve(`+response.data.id+`, 2, '`+response.data.nama_pimpinan+`', '`+response.data.no_wa+`')" class="btn btn-danger tx-13">Tolak</button>
            <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Tutup</button>
          `);
        } else if (response.data.id_status == 3) {
          $('#modal_footer').html(`
            <button type="button" onclick="approve(`+response.data.id+`, '0', '`+response.data.nama_pimpinan+`', '`+response.data.no_wa+`')" class="btn btn-success tx-13">Verifikasi</button>
            <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Tutup</button>
          `);
        } else {
          $('#modal_footer').html(`
            <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Tutup</button>
          `);
        }
        $('#modal_data').modal('show');
      } else {
        alert('failed');
      }
    }, 
  });
}

function approve(id, approve, nama, wa){
  var txt = approve == 1 ? 'Anda yakin menyetujui agent ini?' : approve == 2 ? 'Anda yakin menolak agent ini?' : 'Anda yakin ingin membantu verifikasi data agent ini?';
  if (confirm(txt)) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Agent/approve/_',
      data: {
        id        : id,
        id_status : approve,
        id_admin  : adminID,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(responses) {
        if (responses.code == 200) {
          if (approve == 1) {
            $('#modal_data').modal('hide');
            $('#modal_tampil').modal({backdrop: 'static', keyboard: false});
            $('#modal_footer2').html(`
              <button type="button" onclick="tampil_agent(`+id+`, 0, '`+nama+`', '`+wa+`')" class="btn btn-success tx-13">Ya</button>
              <button type="button" onclick="tampil_agent(`+id+`, 1, '`+nama+`', '`+wa+`')" class="btn btn-danger tx-13">Tidak</button>
            `);
          } else {
            $('#modal_data').modal('hide');
            get_data();
          }
        } else {
          alert('failed');
        }
      }, 
    });
  }
}

function tampil_agent(id, tampil, nama, wa){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Dashboard/tampil_agent/_',
      data: {
        id        : id,
        id_tampil : tampil,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(responses) {
        if (responses.code == 200) {
          sendWA(nama, wa, '0');
        } else {
            alert('failed');
        }
      }, 
    });
}

function sendWA(nama, wa, setuju){
  var whatsapp = wa.replace(/[-+]/g,'');

  if (whatsapp.slice(0,2) == '62'){
    var phone = whatsapp;
  } else {
    var phone = '62'+whatsapp;
  }

  var message;
  if (setuju == '0') {
    message = `Yth Bapak / Ibu `+nama+`,\n\nTerima kasih atas pendaftaran anda menjadi anggota ASITA Jawa Timur. Data anda telah kami setujui. Dan dengan ini anda resmi menjadi anggota ASITA Jawa Timur.\n\nInfo lebih lanjut hubungi Administrasi ASITA Jawa Timur.\n\nTerima kasih,\nASIA JAWA TIMUR\n\n_Pesan otomatis dari sistem, jangan balas pesan ini._`;
  } else {
    message = `Yth Bapak / Ibu `+nama+`,\n\nTerima kasih atas pendaftaran anda menjadi anggota ASITA Jawa Timur. Saat ini permohonan anda belum dapat kami setujui.\n\nInfo lebih lanjut hubungi Administrasi ASITA Jawa Timur.\n\nTerima kasih,\nASIA JAWA TIMUR\n\n_Pesan otomatis dari sistem, jangan balas pesan ini._`;
  }

  $.ajax({
    type: 'POST',
    url: 'https://tghapisystem.net/wablash/module/single_message',
    dataType: 'json',
    data: {
      'phone'   : phone,
      'message' : message,
    },
    success: function(responses) {
      if(responses.status==true){
        $('#modal_tampil').modal('hide');
        $('#modal_data').modal('hide');
        get_data();
      }else{
        alert(responses.message);
        $('#modal_tampil').modal('hide');
        $('#modal_data').modal('hide');
        get_data();
      }
    }
  });
}

function sendWA_verifikasi(gelar, nama, wa, nama_perusahaan, nomor_anggota, alamat, kodepos, telp, email, email_pimpinan, code){
  var whatsapp = wa.replace(/[-+]/g,'');

  if (whatsapp.slice(0,2) == '62'){
    var phone = whatsapp;
  } else {
    var phone = '62'+whatsapp;
  }

  var Vnama_perusahaan 	= nama_perusahaan == '' ? '-' : nama_perusahaan;
  var Vnomor_anggota 	= nomor_anggota == '' ? '-' : nomor_anggota;
  var Valamat 			= alamat == '' ? '-' : alamat;
  var Vkodepos			= kodepos == '' ? '-' : kodepos;
  var Vtelp 			= telp == '' ? '-' : telp;
  var Vemail 			= email == '' ? '-' : email;
  var Vemail_pimpinan 	= email_pimpinan == '' ? '-' : email_pimpinan;

  if (confirm('Anda yakin akan mengirim ulang whatsapp untuk agent ini?')) {
    $.ajax({
      type: 'POST',
      url: 'https://tghapisystem.net/wablash/module/single_message',
      dataType: 'json',
      data: {
        'phone'   : phone,
        // 'message' : `Yth Bapak / Ibu `+nama+`, terima kasih telah registrasi di Asita Jatim.\n\nIni adalah detail data Anda :\nNama Perusahaan : `+nama_perusahaan+`\nNomor Anggota : `+nomor_anggota+`\nAlamat : `+alamat+`\nKode Pos : `+kodepos+`\nNomor Telepon : `+telp+`\nEmail : `+email+`\n\nKlik link berikut untuk memastikan bahwa data yang ada inputkan valid\nhttps://asitajatim.org/verifikasi/`+code+`\nTerima kasih\n\nPesan otomatis dari sistem, jangan balas pesan ini.`,
      	'message'	: ``+gelar+` `+nama+`,\nTerima kasih telah melakukan registrasi di website Asita Jatim.\n\nBerikut adalah data yang telah anda input :\nNama Perusahaan :\n*`+Vnama_perusahaan+`*\nNomor Anggota Asita :\n*`+Vnomor_anggota+`*\nAlamat :\n*`+Valamat+`*\nKode Pos :\n*`+Vkodepos+`*\nNomor Telepon :\n*`+Vtelp+`*\nEmail :\n*`+Vemail+`*\n\nNama Pimpinan/Pemilik :\n*`+nama_pimpinan+`*\nEmail Pimpinan / Pemilik :\n*`+Vemail_pimpinan+`*\nNo Whatsapp Pimpinan/Pemilik :\n*`+no_wa+`*\n\nTerima kasih anda telah mendukung :\n1.   Mengembalikan ASITA 1971\n2.   Musyawarah 12 Agustus 2020 baik dengan hadir maupun melalui zoom.\n3.   Memberikan donasi untuk Pelaksanaan Kegiatan minimal sebesar Rp. 50,000 yang ditransfer ke rekening BCA 7210 1445 14 atasnama EVA NURITA {Konfirmasikan donasi anda ke : 081 2329 2220}\n\nSilakan klik link dibawah ini bila anda menyetujui 3 hal diatas :\nhttps://asitajatim.org/verifikasi/`+code+`\n\nTerima kasih.\nPesan ini terkirim secara otomatis dari system Asita Jatim, mohon tidak di reply`,
      },
      success: function(responses) {
        if(responses.status==true){
          get_data();
        }else{
          alert(responses.message);
          get_data();
        }
      }
    });
  }
}

function masterCity(){
  $.ajax({
    type: 'GET',
    url: configUrl + 'Agent/masterCity/_',
    dataType: 'json',
    success: function(responses) {
      $('#id_kota').empty();
      if(responses.status == true){
        $.each(responses.data, function(a, b){
          $('#id_kota').append(`<option value="`+ b.id_kab +`">`+ b.nama +`</option>`);
        });
      }else{
        alert('Error! Unable to load data city');
      }
    }
  });
}

function edit(id){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Agent/get_details/_',
    data: {
      id : id
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.status == true) {
        $('#id').val(response.data.id);
        $('#nama_perusahaan').val(response.data.nama_perusahaan);
        $('#nama_pt').val(response.data.nama_pt);
        $('#no_anggota').val(response.data.no_anggota);
        $('#alamat').val(response.data.alamat);
        $('#alamat_2').val(response.data.alamat_2);
        $('#provinsi').val(response.data.provinsi);
        $('#id_provinsi').val(response.data.id_provinsi);
        $('#id_kota').val(response.data.id_kota);
        $('#telp').val(response.data.telp);
        $('#email').val(response.data.email);
        $('#gelar_pimpinan').val(response.data.gelar_pimpinan);
        $('#nama_pimpinan').val(response.data.nama_pimpinan);
        $('#email_pimpinan').val(response.data.email_pimpinan);
        $('#no_wa').val(response.data.no_wa);
        $('#file_kartu_nama').val(response.data.file_kartu_nama);
        $('#id_tampil').val(response.data.id_tampil);
        if (response.data.file_kartu_nama == '' || response.data.file_kartu_nama == null) {
          $('#txt_img').html(`<font class="tx-12 tx-medium tx-teal">-- No file uploaded --</font>`);
        } else {
          $('#txt_img').html(`
            <a href="http://asitajatim.org/files/namecard/`+ response.data.file_kartu_nama +`" class="tx-12 tx-medium tx-indigo" target="_blank" rel="noreferrer">
              <i class="fas fa-search mg-l-5"></i> View
            </a>
            <a href="javascript:;" onclick="removeImg()" class="tx-12 tx-medium tx-danger">
              <i class="fas fa-trash mg-l-5"></i> Remove
            </a>
          `);
        }
        $('#modal_edit').modal('show');
      } else {
        alert('failed');
      }
    }, 
  });
}

function save(id){
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Agent/save/_',
    data: $('#form_master').serialize(),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
        $('#modal_edit').modal('hide');
        get_data()
      } else {
        alert('Failed, try again later.');
        $('#modal_edit').modal('hide');
        get_data()
      }
    }, 
  });
}

function uploadImg(idname,type){
  if (($("#"+idname))[0].files.length > 0) {
      var file = $("#"+idname)[0].files[0];
      var formdata = new FormData();
      formdata.append("img", file);
      formdata.append("type", type);
      var ajax = new XMLHttpRequest();
      ajax.addEventListener("load", completeHandler, false);
      ajax.open("POST", configUrl + "Agent/uploadImg/_");
      ajax.send(formdata);
  } else {
      alert("No file chosen!");
  }
}

function completeHandler(event){
  var data = event.target.responseText;
  var jsonResponse = JSON.parse(data);
  if(jsonResponse["status"] == true){
    alert('file uploaded');
    $('#file_kartu_nama').val(jsonResponse["url"]);
    $('#txt_img').html(`
      <a href="http://asitajatim.org/files/namecard/`+ $('#file_kartu_nama').val() +`" class="tx-12 tx-medium tx-indigo" target="_blank" rel="noreferrer">
        <i class="fas fa-search mg-l-5"></i> View
      </a>
      <a href="javascript:;" onclick="removeImg()" class="tx-12 tx-medium tx-danger">
        <i class="fas fa-trash mg-l-5"></i> Remove
      </a>
    `);
  }else{
    alert(jsonResponse["message"]);
  }
}