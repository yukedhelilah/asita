$(document).ready(function() {
  $('.nav-link').css('color','#001737');
  $('#nav-opini').css('color','#0168fa');
  get_data();
});

function get_data(){
  $('#main-table > tbody').empty();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Opini/get_data/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      var aaData = [];
      $.each(response.data, function( i, value ) {
        var stringData  = value._;
        var no          = i + 1;

        var gambar  = value.gambar == '' ? '-' : `<a href="http://asitajatim.org/server/files/postimage/`+ value.gambar +`" class="tx-12 tx-medium tx-indigo" target="_blank" rel="noreferrer">Lihat Gambar</a>`;
        var video   = value.video == '' ? '-' : `<a href="`+ value.video +`" class="tx-12 tx-medium tx-indigo" target="_blank" rel="noreferrer">Lihat Video</a>`;

        aaData.push([
          `<div style="text-align:center">` + no + `</div>`,
          value.judul,
          gambar,
          video,
          `<div style="text-align:center; color:#0168fa">
            <a href="opini_edit?search=`+btoa(value.id)+`" class="tx-12 tx-medium" data-toggle="tooltip" title="Edit" style="cursor:pointer"><i class="fas fa-edit"></i></a>
            <font class="tx-12 tx-info">|</font>
            <a class="tx-12 tx-medium" onclick="delete_data(`+value.id+`)" data-toggle="tooltip" title="Delete" style="cursor:pointer"><i class="fas fa-trash"></i></a>
          </div>`,
        ]);
      });
      $('#main-table').DataTable().destroy();
      $('#main-table').DataTable({
      // 'responsive': true,
        'language'  : {
          'searchPlaceholder'   : 'Search...',
          'sSearch'       : '',
          'lengthMenu'      : '_MENU_ items/page',
          },
        'aaData'  : aaData,
        "bJQueryUI" : true,
        "aoColumns" : [
          { "sWidth"  : "1%" },  
          { "sWidth"  : "40%" }, 
          { "sWidth"  : "10%" }, 
          { "sWidth"  : "10%" }, 
          { "sWidth"  : "8%" }, 
        ],
        "aLengthMenu": [[10, 20, 50, 100],[ 10, 20, 50, 100]],
      });
    },
  });
}

function delete_data(id){
  if(confirm("Anda yakin mengapus opini ini?")){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Opini/delete/_',
      data: {
        id : id
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(responses) {
        if (responses.code == 200) {
          get_data();
        } else {
          alert('failed');
        }
      }, 
    });
  }
}