var adminID     = atob(localStorage.getItem(btoa("asita_id")));
var name     	= atob(localStorage.getItem(btoa("asita_name")));
var username    = atob(localStorage.getItem(btoa("asita_username")));
var token    	= atob(localStorage.getItem(btoa("asita_token")));
var expired     = localStorage.getItem(btoa("asita_expired"));
var exp         = new Date(atob(expired));
var now         = new Date();
var configUrl   = 'http://asitajatim.org/server/Admin/';

var bulans          = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
var days            = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

if(name == null || username == null || token == null || expired == null || exp.getDate() +`-`+ exp.getMonth() +`-`+ exp.getFullYear() != now.getDate() +`-`+ now.getMonth() +`-`+ now.getFullYear()){
    removeLocalstorage();
    window.location.href = '../pages/login';
}

$(document).ready(function() {
    $('#txt_session').html(name);
});

function dateFormat(date) {
    var format = new Date(date);
    var date = format.getDate().toString().length == 2 ? format.getDate() : `0`+format.getDate();
    return date + ' ' + bulans[format.getMonth()] + ' ' + format.getFullYear().toString().substr(2,2);
}

function dateTimeFormat(d) {
    var t       = new Date(d);
    var hours   = t.getHours() < 10 ? `0`+t.getHours() : t.getHours();
    var minutes = t.getMinutes() < 10 ? `0`+t.getMinutes() : t.getMinutes(); 

    return t.getDate() + ` ` + bulans[t.getMonth()] + ` ` + t.getFullYear().toString().substr(2,2) + ` [ ` + hours + `:` + minutes + ` ]`;
}

function logout() {
    if (confirm("Anda yakin ingin keluar?")) {
        removeLocalstorage();
        window.location.href = '../pages/login';
    } 
}

function removeLocalstorage(){
    localStorage.removeItem(btoa('asita_id'));
    localStorage.removeItem(btoa('asita_name'));
    localStorage.removeItem(btoa('asita_username'));
    localStorage.removeItem(btoa('asita_token'));
    localStorage.removeItem(btoa('asita_expired'));
}