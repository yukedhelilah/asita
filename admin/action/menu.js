$(document).ready(function() {
    $('.nav-link').css('color','#001737');
    $('#nav-menu').css('color','#0168fa');
    get_data();
});

function get_data(){
  $('#main-table > tbody').empty();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Dashboard/menu/_',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      $('#jumlah').html(response.data.jumlah);

      var tampil = response.data.tampil == 0 ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
      $('#tampil').html(tampil);
      
      if (response.data.tampil == 0) {
        $('#tampil1').css('display','block');
        $('#tampil0').css('display','none');
      } else {
        $('#tampil0').css('display','block');
        $('#tampil1').css('display','none');
      }

      var aaData = [];
      $.each(response.data.jumlah, function( i, value ) {
          var stringData  = value._;
          var no          = i + 1;

          aaData.push([
        `<div style="text-align:center">` + no + `</div>`,
        dateTimeFormat(value.tgl_registrasi),
        value.nama_perusahaan.toUpperCase(),
        value.alamat,
        value.kota,
        value.nama_pimpinan,
        `62`+value.no_wa,
        `<div style="text-align:center; color:#0168fa">
          <a onclick="get_details(`+value.id+`)" class="tx-12 tx-medium" data-toggle="tooltip" title="Details" style="cursor:pointer"><i class="fas fa-search"></i></a>
        </div>`,
        ]);
      });
      $('#main-table').DataTable().destroy();
      $('#main-table').DataTable({
        // 'responsive': true,
          'language'  : {
            'searchPlaceholder'   : 'Search...',
            'sSearch'       : '',
            'lengthMenu'      : '_MENU_ items/page',
            },
          'aaData'  : aaData,
          "bJQueryUI" : true,
          "aoColumns" : [
            { "sWidth"  : "1%" },  
            { "sWidth"  : "10%" }, 
            { "sWidth"  : "15%" }, 
            { "sWidth"  : "15%" }, 
            { "sWidth"  : "10%" }, 
            { "sWidth"  : "15%" }, 
            { "sWidth"  : "10%" }, 
            { "sWidth"  : "1%" }, 
          ],
          "aLengthMenu": [[10, 20, 50, 100],[ 10, 20, 50, 100]],
      });
    },
  });
}

function tampil(id_tampil){
  var txt = id_tampil == 1 ? "Anda yakin agent tidak ditampilkan di halaman depan?" : "Anda yakin agent ditampilkan di halaman depan?";
  if (confirm(txt)) {
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: configUrl + 'Dashboard/tampil/_',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data : {
          id_tampil : id_tampil,
        },
        success: function(response) {
          if (response.code == 200) {
            get_data();
          } else {
            alert('Failed, try again later.');
          }
        },
      });
  }
}

function get_details(id){
  $('#tb_data').empty();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Agent/get_details/_',
    data: {
      id : id
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.status == true) {
        var namecard = response.data.file_kartu_nama == '' ? '<small style="color:red">Tidak ada kartu nama</small>' : `<a href="http://asitajatim.org/files/namecard/`+ response.data.file_kartu_nama +`" target="_blank">lihat gambar</a>`;
        $('#tb_data').append(`
          <tr>
            <td>Nama Perusahaan</td>
            <th>`+ response.data.nama_perusahaan +`</th>
          </tr>
          <tr>
            <td>Nama PT</td>
            <th>`+ response.data.nama_pt +`</th>
          </tr>
          <tr>
            <td>Nomor Anggota</td>
            <th>`+ response.data.no_anggota +`</th>
          </tr>
          <tr>
            <td>Alamat</td>
            <th>`+ response.data.alamat +`</th>
          </tr>
          <tr>
            <td>Alamat 2</td>
            <th>`+ response.data.alamat_2 +`</th>
          </tr>
          <tr>
            <td>Provinsi</td>
            <th>`+ response.data.provinsi +`</th>
          </tr>
          <tr>
            <td>Kota</td>
            <th>`+ response.data.kota +`</th>
          </tr>
          <tr>
            <td>No Telp</td>
            <th>`+ response.data.telp +`</th>
          </tr>
          <tr>
            <td>Email</td>
            <th>`+ response.data.email +`</th>
          </tr>
          <tr>
            <td>Nama Pimpinan</td>
            <th>`+ response.data.gelar_pimpinan +`. `+ response.data.nama_pimpinan +`</th>
          </tr>
          <tr>
            <td>Email Pimpinan</td>
            <th>`+ response.data.email_pimpinan +`</th>
          </tr>
          <tr>
            <td>Nomor Whatsapp</td>
            <th>`+ response.data.no_wa +`</th>
          </tr>
          <tr>
            <td>Kartu Nama</td>
            <th>`+ namecard +`</th>
          </tr>
        `);
        $('#modal_footer').html(`
          <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Tutup</button>
        `);
        $('#modal_data').modal('show');
      } else {
        alert('failed');
      }
    }, 
  });
}

function approve(id, approve, nama, wa){
  var txt = approve == 1 ? 'Anda yakin menyetujui agent ini?' : 'Anda yakin menolak agent ini?';
  if (confirm(txt)) {
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Agent/approve/_',
      data: {
        id        : id,
        id_status : approve,
        id_admin  : adminID,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(responses) {
        if (responses.code == 200) {
          if (approve == 1) {
            $('#modal_data').modal('hide');
            $('#modal_tampil').modal({backdrop: 'static', keyboard: false});
            $('#modal_footer2').html(`
              <button type="button" onclick="tampil_agent(`+id+`, 0, '`+nama+`', '`+wa+`')" class="btn btn-success tx-13">Ya</button>
              <button type="button" onclick="tampil_agent(`+id+`, 1, '`+nama+`', '`+wa+`')" class="btn btn-danger tx-13">Tidak</button>
            `);
          } else {
              $('#modal_data').modal('hide');
              get_data();
          }
        } else {
          alert('failed');
        }
      }, 
    });
  }
}

function tampil_agent(id, tampil, nama, wa){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: configUrl + 'Dashboard/tampil_agent/_',
      data: {
        id        : id,
        id_tampil : tampil,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function(responses) {
        if (responses.code == 200) {
          sendWA(nama, wa, '0');
        } else {
          alert('failed');
        }
      }, 
    });
}

function sendWA(nama, wa, setuju){
    var whatsapp = wa.replace(/[-+]/g,'');

    if (whatsapp.slice(0,2) == '62'){
        var phone = whatsapp;
    } else {
        var phone = '62'+whatsapp.substring(1);
    }

    var message;
    if (setuju == '0') {
      message = `Yth Bapak / Ibu `+nama+`,\n\nTerima kasih atas pendaftaran anda menjadi anggota ASITA Jawa Timur. Data anda telah kami setujui. Dan dengan ini anda resmi menjadi anggota ASITA Jawa Timur.\n\nInfo lebih lanjut hubungi Administrasi ASITA Jawa Timur.\n\nTerima kasih,\nASIA JAWA TIMUR\n\n_Pesan otomatis dari sistem, jangan balas pesan ini._`;
    } else {
      message = `Yth Bapak / Ibu `+nama+`,\n\nTerima kasih atas pendaftaran anda menjadi anggota ASITA Jawa Timur. Saat ini permohonan anda belum dapat kami setujui.\n\nInfo lebih lanjut hubungi Administrasi ASITA Jawa Timur.\n\nTerima kasih,\nASIA JAWA TIMUR\n\n_Pesan otomatis dari sistem, jangan balas pesan ini._`;
    }

    $.ajax({
        type: 'POST',
        url: 'https://tghapisystem.net/wablash/module/single_message',
        dataType: 'json',
        data: {
            'phone'   : phone,
            'message' : message,
        },
        success: function(responses) {
            if(responses.status==true){
            $('#modal_tampil').modal('hide');
            $('#modal_data').modal('hide');
                get_data();
            }else{
                alert(responses.message);
            $('#modal_tampil').modal('hide');
            $('#modal_data').modal('hide');
                get_data();
            }
        }
    });
}

function agent(status){
    window.location.href = '../pages/agent?search='+btoa(status);
}