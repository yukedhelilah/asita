$(function(){
  'use strict'

  $(".editor").each(function () {
    let id = $(this).attr('id');
    CKEDITOR.replace(id);
  });

  $("#file_img1").change(function () {
    uploadImg(1);
  });

  // $("#file_img2").change(function () {
  //   uploadImg(2);
  // });

  setTimeout(function () {
    loadImg();
  }, 500);
});

function uploadImg(no){
  if (($("#file_img"+no))[0].files.length > 0) {
    var file = $("#file_img"+no)[0].files[0];
    var formdata = new FormData();
    formdata.append("file_img", file);
    formdata.append("no", no);
    var ajax = new XMLHttpRequest();
    ajax.addEventListener("load", completeHandler2, false);
    ajax.open("POST", configUrl + "Opini/uploadImg/_");
    ajax.send(formdata);
  } else {
    alert("No file chosen!");
  }
}

function completeHandler2(event){
  var data = event.target.responseText.split('*');
  var no = data[2];
  if(data[0]!=''){
    $('#file_img'+no).val('');
    alert("Error! "+ data[1]);
  }else{
    $('#img'+no).val(data[1]);
    loadImg();
    $('#file_img'+no).val('');
  }   
}

function loadImg(){
  if($('#img1').val() == ''){
    $('#txt_img1').html(`<font class="tx-12 tx-medium tx-teal">-- No file uploaded --</font>`);
  }else{
    $('#txt_img1').html(`
      <a href="http://asitajatim.org/server/files/postimage/`+ $('#img1').val() +`" class="tx-12 tx-medium tx-indigo" target="_blank" rel="noreferrer">
        <i class="fas fa-search mg-l-5"></i> View
      </a>
      <a href="javascript:;" onclick="removeImg(1)" class="tx-12 tx-medium tx-danger">
        <i class="fas fa-trash mg-l-5"></i> Remove
      </a>
    `);
  } 

  // if($('#img2').val() == ''){
  //   $('#txt_img2').html(`<font class="tx-12 tx-medium tx-teal">-- No file uploaded --</font>`);
  // }else{
  //   $('#txt_img2').html(`
  //     <a href="http://asitajatim.org/server/files/postvideo/`+ $('#img2').val() +`" class="tx-12 tx-medium tx-indigo" target="_blank" rel="noreferrer">
  //       <i class="fas fa-search mg-l-5"></i> View
  //     </a>
  //     <a href="javascript:;" onclick="removeImg(2)" class="tx-12 tx-medium tx-danger">
  //       <i class="fas fa-trash mg-l-5"></i> Remove
  //     </a>
  //   `);
  // } 
}

function removeImg(no){
  $('#img'+no).val('');
  loadImg();
}

function actionSave() {
  for (instance in CKEDITOR.instances){
    CKEDITOR.instances[instance].updateElement();
  }
  
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: configUrl + 'Opini/save/_',
    data: $('#form_master').serialize()+"&id_admin="+adminID,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    success: function(response) {
      if (response.code == 200) {
      $('#add_alert_success').css('display','block');
        $("html, body").animate({ scrollTop: 0 }, "slow");
        document.getElementById('form_master').reset();
        CKEDITOR.instances['deskripsi'].setData('');
      } else {
        $('#add_alert_failed').css('display','block');
        $("html, body").animate({ scrollTop: 0 }, "slow");
      }
    },
  });
}