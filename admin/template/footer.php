    <!-- <footer class="footer">
      <div>
        <span>&copy; 2019 DashForge v1.0.0. </span>
        <span>Created by <a href="http://themepixels.me">ThemePixels</a></span>
      </div>
      <div>
        <nav class="nav">
          <a href="https://themeforest.net/licenses/standard" class="nav-link">Licenses</a>
          <a href="../change-log.html" class="nav-link">Change Log</a>
          <a href="https://discordapp.com/invite/RYqkVuw" class="nav-link">Get Help</a>
        </nav>
      </div>
    </footer> -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/feather-icons/feather.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/jquery.flot/jquery.flot.js"></script>
    <script src="../lib/jquery.flot/jquery.flot.stack.js"></script>
    <script src="../lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="../lib/chart.js/Chart.bundle.min.js"></script>
    <script src="../lib/jqvmap/jquery.vmap.min.js"></script>
    <script src="../lib/jqvmap/maps/jquery.vmap.usa.js"></script>
    <script src="../lib/quill/quill.min.js"></script>

    <script src="../lib/prismjs/prism.js"></script>
    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="../lib/select2/js/select2.full.min.js"></script>

    <script src="../lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
    <script src="../lib/typeahead.js/typeahead.bundle.min.js"></script>
    <script src="../lib/parsleyjs/parsley.min.js"></script>
    <script src="../lib/jqueryui/jquery-ui.min.js"></script>

    <script src="../assets/js/dashforge.js"></script>
    <script src="../assets/js/dashforge.sampledata.js"></script>

    <!-- append theme customizer -->
    <script src="../lib/js-cookie/js.cookie.js"></script>
    <!-- <script src="../assets/js/dashforge.settings.js"></script> -->

    <script src="../function.js" type="text/javascript"></script>
    <script src="../action/wholejs.js" type="text/javascript"></script>
    <script src="../assets/js/notify.js"></script>
    <script src="../assets/js/notify.min.js"></script>
  </body>
</html>
