<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<?php
				$menu = [
					[ 'name' => 'Dashboard', 'icon' => 'fa fa-home fa-fw', 'file' => 'home' ],
					[ 'name' => 'Form', 'icon' => 'fa fa-edit', 'file' => 'form' ],
				];
				
				foreach($menu as $key => $value) {
					echo '<li>
							<a href="'.$value['file'].'">
								<i class="'.$value['icon'].'"></i> '.$value['name'].'
							</a>
						</li>';
				}
			?>
			<li>
			<a href="#"><i class="fa fa-table"></i> Table </a>
				<ul class="nav nav-second-level">
					<li>
						<a href="table">Basic</a>
					</li>
					<li>
						<a href="table-datatable">Datatables</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li>
						<a href="chart-flot">Flot Charts</a>
					</li>
					<li>
						<a href="chart-morris">Morris.js Charts</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li>
						<a href="panels-wells">Panels and Wells</a>
					</li>
					<li>
						<a href="buttons">Buttons</a>
					</li>
					<li>
						<a href="notifications">Notifications</a>
					</li>
					<li>
						<a href="typography">Typography</a>
					</li>
					<li>
						<a href="icons"> Icons</a>
					</li>
					<li>
						<a href="grid">Grid</a>
					</li>
				</ul>
			</li>
			<li>
			<a href="#"><i class="fa fa-file"></i> Pages </a>
				<ul class="nav nav-second-level">
					<li>
						<a href="login">Login</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>