<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="http://asitajatim.org/assets/img/favicon.ico">

    <title>ASITA | ADMIN</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../lib/jqvmap/jqvmap.min.css" rel="stylesheet">

    <link href="../lib/typicons.font/typicons.css" rel="stylesheet">
    <link href="../lib/prismjs/themes/prism-vs.css" rel="stylesheet">
    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <link href="../lib/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <link href="../lib/quill/quill.core.css" rel="stylesheet">
    <link href="../lib/quill/quill.snow.css" rel="stylesheet">
    <link href="../lib/quill/quill.bubble.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="../assets/css/dashforge.css">
    <link rel="stylesheet" href="../assets/css/dashforge.dashboard.css">
    <link rel="stylesheet" href="../assets/css/dashforge.profile.css">
    <link rel="stylesheet" href="../assets/css/wholecss.css">

    <style type="text/css">
      .select_xs{
        width: 5.5em;
      }
    </style>
  </head>
  <body class="page-profile" style="background-color: #f6f6f6">

    <header class="navbar navbar-header navbar-header-fixed">
      <a href="javascript:'" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
      <a href="dashboard" class="df-logo pd-x-15">ASITA<span>&nbsp| ADMIN</span></a>
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
          <a href="home" class="df-logo">ASITA<span>&nbsp| ADMIN</span></a>
          <a id="mainMenuClose" href="javascript:'"><i data-feather="x"></i></a>
        </div>
        <ul class="nav navbar-menu">
          <li class="nav-label pd-l-20 pd-lg-l-25 d-lg-none">Main Navigation</li>
          <li class="nav-item"><a href="dashboard" class="nav-link" id="nav-dashboard"><i data-feather="home"></i> DASHBOARD</a></li>
          <li class="nav-item"><a href="agent?search=OTk=" class="nav-link" id="nav-agent"><i data-feather="user"></i> AGENTS</a></li>
          <li class="nav-item"><a href="opini" class="nav-link" id="nav-opini"><i data-feather="bookmark"></i> OPINI</a></li>
          <li class="nav-item"><a href="menu" class="nav-link" id="nav-menu"><i data-feather="settings"></i> PENGATURAN MENU</a></li>
          <li class="nav-item with-sub">
            <a href="" class="nav-link" id="nav-master"><i data-feather="database"></i> MASTER</a>
            <div class="navbar-menu-sub">
              <div class="d-lg-flex">
                <ul class="wd-100">
                  <li class="nav-sub-item"><a href="admin" class="nav-sub-link"><i data-feather="users"></i>Admin</a></li>
                </ul>
              </div>
            </div>
          </li>
        </ul>
      </div><!-- navbar-menu-wrapper -->
      <div class="navbar-right">
        <div class="dropdown dropdown-profile">
          <a href="" class="dropdown-link" data-toggle="dropdown" data-display="static">
            <div class="avatar avatar-sm"><img src="../assets/img/user.png" class="rounded-circle" alt=""></div>
          </a><!-- dropdown-link -->
          <div class="dropdown-menu dropdown-menu-right tx-13">
            <div class="avatar avatar-lg mg-b-15"><img src="../assets/img/user.png" class="rounded-circle" alt=""></div>
            <h6 class="tx-semibold mg-b-5" id="txt_session">...</h6>
            <p class="mg-b-25 tx-12 tx-color-03">Admin</p>
            <div class="dropdown-divider"></div>
            <a href="javascript:;" onclick="logout()" class="dropdown-item"><i data-feather="log-out"></i>Log Out</a>
          </div><!-- dropdown-menu -->
        </div>
      </div>
    </header><!-- navbar -->