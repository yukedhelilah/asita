<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="http://asitajatim.org/assets/img/favicon.ico">

    <title>ASITA | ADMIN</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../lib/typicons.font/typicons.css" rel="stylesheet">
    <link href="../lib/prismjs/themes/prism-vs.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="../assets/css/dashforge.css">
    <link rel="stylesheet" href="../assets/css/dashforge.auth.css">
  </head>
  <body>

    <header class="navbar navbar-header navbar-header-fixed">
      <div class="navbar-brand" style="padding-top: 5px !important">
        <a href="login" class="df-logo">ASITA<span>&nbsp| ADMIN</span></a>
      </div>
    </header>

    <div class="content content-fixed content-auth">
      <div class="container" style="top: 50%;left:50%;transform: translate(-50%, -50%);position: absolute;">
        <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
          <div class="media-body align-items-center d-none d-lg-flex">
            <div class="mx-wd-600">
              <img src="../assets/img/img06.png" class="img-fluid" alt="">
            </div>
            <div class="pos-absolute b-0 l-0 tx-12 tx-center">
            </div>
          </div><!-- media-body -->
          <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">
            <div class="wd-100p">
              <h3 class="tx-color-01 mg-b-5">Sign In</h3>
              <p class="tx-color-03 tx-16 mg-b-40">Welcome back Administrator! Please sign in to continue.</p>

              <div class="alert alert-danger" role="alert" id="txt_alert" style="display: none;">...</div>
              <div class="alert alert-success" role="alert" id="txt_success" style="display: none;">...</div>

              <form id="form_signin" autocomplete="off" data-parsley-validate>
              <div class="form-group">
                <label>Username</label>
                <input type="text" id="username" name="username" class="form-control" placeholder="Enter your username" autocomplete="off">
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f">Password</label>
                  <!-- <a href="" class="tx-13">Forgot password?</a> -->
                </div>
                <input type="password" id="password" name="password" class="form-control" placeholder="Enter your password" autocomplete="off">
              </div>
              <a class="btn btn-brand-02 btn-block mg-b-20" style="color:white" id="login">Sign In</a>
              <fieldset class="form-fieldset" style="text-align: center;">
                Forgot password? <a href="forgot_password">Reset</a>
              </fieldset>
              </form>
            </div>
          </div><!-- sign-wrapper -->
        </div><!-- media -->
      </div><!-- container -->
    </div><!-- content -->

    <footer class="footer">
      <div>
        <span>&copy; 2020 ASITA JATIM (asitajatim.org) </span>
        <span>Created by <a href="javascript:;">Creatrix Software</a></span>
      </div>
      <div>
        <nav class="nav">
          <a href="javascript:;" class="nav-link"></a>
          <a href="javascript:;" class="nav-link"></a>
          <a href="javascript:;" class="nav-link"></a>
        </nav>
      </div>
    </footer>

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/feather-icons/feather.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/prismjs/prism.js"></script>

    <!-- <script src="../assets/js/dashforge.js"></script> -->

    <!-- append theme customizer -->
    <script src="../lib/js-cookie/js.cookie.js"></script>
    <script src="../action/login.js" type="text/javascript"></script>
    <!-- <script src="../assets/js/dashforge.settings.js"></script> -->

  </body>
</html>