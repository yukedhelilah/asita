<?php include "../template/header.php"; ?>

<div class="content content-fixed">
	<div class="container-fluid pd-x-0 pd-lg-x-10 pd-xl-x-0">
  		<div class="row row-xs">
	        <div class="col-sm-12 col-lg-2 mg-t-10 mg-lg-t-0">
	          <div class="card card-body" style="padding: 20px 10px">
	            <h6 class="tx-uppercase tx-15 tx-spacing-1 tx-color-02 tx-semibold mg-b-0" style="text-align: center;">TAMPILKAN AGENT</h6>
	            <small style="text-align: center;margin-bottom: 7px">Menampilkan di halaman depan</small>
	            <div>
	              <h1 class="tx-purple tx-rubik mg-b-0 mg-r-5 lh-1" id="tampil" style="text-align: center;font-weight: bold;font-size: 50px"></h1>
	            </div>
	            <button class="btn btn-outline-primary btn-xs mg-t-10" onclick="tampil(0)" id="tampil0">Tampilkan</button>
	            <button class="btn btn-outline-danger btn-xs mg-t-10" onclick="tampil(1)" id="tampil1">Sembunyikan</button>
	          </div>
	        </div>
	        <div class="col-sm-12 col-lg-10 mg-t-10 mg-lg-t-0">
		        <div class="card">
		          <div class="card-body">
		          	<h5 class="mg-b-20">LIST AGENT YANG TAMPIL DI HALAMAN DEPAN</h5>
		            <table id="main-table" class="table" style="width: 100%">
		              <thead style="font-size: 11px">
		                <th style="text-align: center;">NO</th>
		                <th>REGISTRASI</th>
		                <th>PERUSAHAAN</th>
		                <th>ALAMAT</th>
		                <th>KOTA</th>
		                <th>PIMPINAN</th>
		                <th>WHATSAPP</th>
		                <th style="text-align: center;">ACT</th>
		              </thead>
		              <tbody style="font-size: 11px">
		              </tbody>
		            </table>
		          </div>
		        </div>
	        </div>
      	</div>

	    <div class="row row-xs mg-t-10">
	      <div class="col-lg-12 col-xl-12 mg-t-12">
	      </div>
	    </div>
  	</div>
</div>

<div class="modal fade" id="modal_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document" style="width: 500px;max-width: 500px">
    <div class="modal-content tx-14">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel5">Agent</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table" style="width: 100%;margin-bottom: 0">
          <tbody id="tb_data">
          </tbody>
        </table>
      </div>
      <div class="modal-footer" id="modal_footer">
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_tampil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content tx-14">
      <div class="modal-body">
        <p class="mg-b-0">Apakah anda ingin menampilkan agent ini di halaman depan?</p>
      </div>
      <div class="modal-footer" id="modal_footer2">
      </div>
    </div>
  </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/menu.js"></script>

<style type="text/css">
  select[name="main-table_length"] {
    border: 1px solid rgb(226, 229, 237);
    padding: 7px 10px;
    border-radius: 5px; 
  }

  .select_xs{
    width: 60px;
  }
</style>