<?php include "../template/header.php"; ?>

<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-0 pd-xl-x-0" style="max-width: none">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="javascript:;">Master</a></li>
            <li class="breadcrumb-item active" aria-current="page">Admin</li>
          </ol>
          <h4 class="mg-b-0 tx-spacing--1">ADMIN</h4>
          <small></small>
        </nav>
      </div>
      <div class="d-none d-md-block">
        <a href="javascript:;" onclick="add_data()" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="plus-circle" class="wd-10 mg-r-5"></i> Tambah Admin</a>
        <button class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5" onclick="get_data()"><i data-feather="refresh-cw" class="wd-10 mg-r-5"></i> Refresh</button>
      </div>
    </div>

    <div class="row row-xs mg-t-10">
      <div class="col-lg-12 col-xl-12 mg-t-12">
        <div class="card">
          <div class="card-body">
            <table id="main-table" class="table" style="width: 100%">
              <thead>
                <th style="text-align: center;">NO</th>
                <th>NAMA</th>
                <th>USERNAME</th>
                <th>JABATAN</th>
                <th>EMAIL</th>
                <th>WHATSAPP</th>
                <th style="text-align: center;">ACT</th>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document" style="width: 400px;max-width: 400px">
    <div class="modal-content tx-14">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel5">Admin</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_master" data-parsley-validate autocomplete="off">
          <input type="hidden" name="act" id="act" value="add">
          <input type="hidden" name="id" id="id" value="">
          <div class="form-group row">
            <label for="nama_admin" class="col-sm-3 col-form-label">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nama_admin" name="nama_admin" placeholder="Nama" />
            </div>
          </div>
          <div class="form-group row">
            <label for="jabatan" class="col-sm-3 col-form-label">Jabatan</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan" />
            </div>
          </div>
          <div class="form-group row">
            <label for="email_admin" class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="email_admin" name="email_admin" placeholder="Email" />
            </div>
          </div>
          <div class="form-group row">
            <label for="wa_admin" class="col-sm-3 col-form-label">Whatsapp</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="wa_admin" name="wa_admin" placeholder="Whatsapp" onKeyUp="requiredField(this.value,'wa_admin')" onblur="requiredField(this.value,'wa_admin')">
              <input type="hidden" name="iswa_admin" id="iswa_admin" value="0">
            </div>
          </div>
          <hr>
          <div class="form-group row">
            <label for="username_admin" class="col-sm-3 col-form-label">Username</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="username_admin" name="username_admin" placeholder="Username" onKeyUp="requiredField(this.value,'username_admin')" onblur="requiredField(this.value,'username_admin')">
              <input type="hidden" id="username_adminCopy">
              <input type="hidden" name="isusername_admin" id="isusername_admin" value="0">
            </div>
          </div>
          <div id="add" class="form-group row" style="margin-bottom: 0 !important;display: none">
            <label for="password_admin" class="col-sm-3 col-form-label">Password</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="password_admin" name="password_admin" placeholder="Password" value="123456" readonly="readonly"/>
            </div>
          </div>
          <div id="edit" class="form-group" style="margin-bottom: 0 !important;display: none">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="resetPassword" name="resetPassword">
              <label class="custom-control-label" for="resetPassword">Reset Password</label>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Batal</button>
        <button type="button" onclick="save()" class="btn btn-primary tx-13">Simpan</button>
      </div>
    </div>
  </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/admin.js" type="text/javascript"></script>

<style type="text/css">
  select[name="main-table_length"] {
    border: 1px solid rgb(226, 229, 237);
    padding: 7px 10px;
    border-radius: 5px; 
  }

  .select_xs{
    width: 60px;
  }

  #nama_admin, #jabatan{
    text-transform: uppercase;
  }
  ::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
  }
  :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
    text-transform: none;
  }
  ::-moz-placeholder { /* Mozilla Firefox 19+ */
    text-transform: none;
  }
  :-ms-input-placeholder { /* Internet Explorer 10+ */
    text-transform: none;
  }
  ::placeholder { /* Recent browsers */
    text-transform: none;
  }
</style>