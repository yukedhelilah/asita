<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Creatrix">
    <meta name="twitter:description" content="Creatrix Marketplace">
    <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/dashforge">
    <meta property="og:title" content="Creatrix">
    <meta property="og:description" content="Creatrix Marketplace">

    <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Creatrix Marketplace">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="http://asitajatim.org/assets/img/favicon.ico">

    <title>ASITA | ADMIN</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="../assets/css/dashforge.css">
    <link rel="stylesheet" href="../assets/css/dashforge.auth.css">
  </head>
  <body>

    <header class="navbar navbar-header navbar-header-fixed">
      <div class="navbar-brand" style="padding-top: 5px !important">
        <a href="login" class="df-logo">ASITA<span>&nbsp| ADMIN</span></a>
      </div>
    </header>

    <div class="content content-fixed content-auth">
      <div class="container">
        <div class="media align-items-stretch justify-content-center ht-100p" style="top: 50%;transform: translateY(-50%);position: sticky;">
          <div class="sign-wrapper mg-lg-r-50 mg-xl-r-60" style="width: 700px">
            <div class="pd-t-20 wd-80p">

              <div class="alert alert-danger" role="alert" id="txt_alert" style="display: none;">...</div>

              <form autocomplete="off" data-parsley-validate="">
                <fieldset class="form-fieldset fieldGroup" id="field1">
                  <legend>RESET PASSWORD</legend>
                  <div class="input-group">
                    <div class="form-group col-sm-12">
                      Please enter your username and we'll send you instruction to reset your password.
                      <input type="text" class="form-control mg-t-10" name="username" id="username" placeholder="Enter your username">
                    </div>
                  </div>
                  <div class="form-group col-sm-12 mg-t-20 mg-b-0" style="text-align: -webkit-center;">
                    <button type="button" class="btn btn-xs btn-primary btn-block" style="width: 40%" onclick="forgotPassword()">Next</button>
                  </div>
                </fieldset>
                <fieldset class="form-fieldset fieldGroup" id="field2" style="display: none">
                  <legend>RESET PASSWORD</legend>
                  <div class="input-group">
                    <div class="form-group col-sm-12">
                      Hi, <span style="font-weight: bold;" id="reset_agentName"></span><br>
                      We've sent a whatsapp massage to <span style="font-weight: bold;" id="reset_whatsappNo"></span><br>
                      and email to <span style="font-weight: bold;" id="reset_email"></span><br>
                      with your authentication code.
                      <input type="text" class="form-control mg-t-10" name="auth" id="auth" placeholder="Enter your authentication code">
                    </div>
                  </div>
                  <div class="form-group col-sm-12 mg-t-20 mg-b-0" style="text-align: -webkit-center;">
                    <button type="button" class="btn btn-xs btn-primary btn-block" style="width: 40%" id="changePassword">Verify Code</button>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
          <div class="media-body pd-y-0 pd-lg-x-0 pd-xl-x-60 align-items-center d-none d-lg-flex pos-relative">
            <div class="mx-lg-wd-500 mx-xl-wd-550">
              <img src="../assets/img/forgot.jpg" class="img-fluid" alt="">
            </div>
            <div class="pos-absolute b-0 r-0 tx-12">
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer class="footer">
      <div>
        <span>&copy; 2020 ASITA JATIM (asitajatim.org) </span>
        <span>Created by <a href="javascript:;">Creatrix Software</a></span>
      </div>
    </footer>

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/feather-icons/feather.min.js"></script>
    <!-- <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script> -->

    <!-- <script src="../assets/js/dashforge.js"></script> -->

    <!-- append theme customizer -->
    <script src="../lib/js-cookie/js.cookie.js"></script>

    <script src="../action/forgot_password.js"></script>
  </body>
</html>

<!-- <style type="text/css">
  #firstName, #lastName, #storeName, #country, #province, #city, #subcity{
    text-transform: uppercase;
  }

  #email, #confirmEmail, #username{
    text-transform: lowercase;
  }

  ::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
  }
  :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
    text-transform: none;
  }
  ::-moz-placeholder { /* Mozilla Firefox 19+ */
    text-transform: none;
  }
  :-ms-input-placeholder { /* Internet Explorer 10+ */
    text-transform: none;
  }
  ::placeholder { /* Recent browsers */
    text-transform: none;
  }

  @media only screen and (min-width: 1300px) {
    .container{
      top: 50%;
      left:50%;
      transform: translate(-50%, -50%);
      position: absolute;
      max-width: 1300px;
    }
  }

  @media only screen and (max-width: 600px) {
    .form-group{
      margin: 5px 0px !important;
    }

    .step-desc{
      display: none !important;
    }

    .steps{
      margin-bottom: 20px;
    }

    #field1, #field2, #field3{
      margin-top: 20px;
    }
  }

  @media only screen and (max-width: 991px) {
    .LogIn{
      display: block !important;
    }
  }
</style> -->
