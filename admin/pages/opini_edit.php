<?php include "../template/header.php"; ?>

<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0" style="max-width: none">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="dashboard">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="opini">Opini</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Opini</li>
          </ol>
          <h4 class="mg-b-0 tx-spacing--1">EDIT OPINI</h4>
          <small></small>
        </nav>
      </div>
      <div class="d-none d-md-block">
        <a class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5" href="opini"><i data-feather="corner-up-left" class="wd-10 mg-r-5"></i> Kembali ke Opini</a>
      </div>
    </div>

    <div class="row row-xs">
      <div class="col-lg-12 col-xl-12 mg-t-12">
        <div id="add_alert_success" style="display: none;">
          <div class="alert alert-primary alert-dismissible fade show" role="alert">
            <strong><i class="far fa-check-circle"></i> Success!</strong> Successfully saved new data.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        </div>
        <div id="add_alert_failed" style="display: none;">
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong><i class="typcn typcn-delete"></i> Error!</strong> Data could not be saved.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <fieldset class="form-fieldset">
              <legend>General Information</legend>
              <form id="form_master" data-parsley-validate>
                <input type="hidden" name="act" id="act" value="edit">
                <input type="hidden" name="id" id="id" value="">
                <div class="form-group row">
                  <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                  <div class="col-sm-5">
                    <input type="text" autocomplete="off" class="form-control" id="judul" name="judul" placeholder="Judul" />
                  </div>
                  <div class="col-sm-5">
                    <!-- <span class="badge badge-info">Note :</span> Maksimal <= 15 karakter -->
                  </div>
                </div>
                <div class="form-group row">
                  <label for="img" class="col-sm-2 col-form-label">Gambar</label>
                  <div class="col-sm-5">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="file_img1" id="file_img1">
                      <label class="custom-file-label" for="img">Choose file</label>
                    </div>
                    <input type="hidden" name="img1" id="img1">
                  </div>
                  <div class="col-sm-5">
                    <div id="txt_img1" class="mg-t-10"></div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="img" class="col-sm-2 col-form-label">Video</label>
                  <div class="col-sm-5">
                    <input type="text" autocomplete="off" class="form-control" id="img2" name="img2" placeholder="Link Youtube" />
                  </div>
                  <!-- <div class="col-sm-5">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="file_img2" id="file_img2">
                      <label class="custom-file-label" for="img">Choose file</label>
                    </div>
                    <input type="hidden" name="img2" id="img2">
                  </div>
                  <div class="col-sm-5">
                    <div id="txt_img2" class="mg-t-10"></div>
                  </div> -->
                </div>
                <div class="form-group row">
                  <label for="deskripsi" class="col-sm-2 col-form-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control editor" rows="4" id="deskripsi" name="deskripsi" placeholder="Description"></textarea>
                  </div>
                </div>
              </form>
            </fieldset>
          </div>
        </div>
        <div class="card mg-t-20">
          <div class="card-body">
            <fieldset class="form-fieldset">
              <legend>Action</legend>
              <div class="form-group">
                <button class="btn btn-primary" type="button" onclick="actionSave()">Simpan</button>
                &nbsp&nbsp&nbspor
                <a href="opini" class="tx-medium"><i class="icon ion-md-arrow-back mg-l-5"></i> Kembali ke Opini</a>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="//cdn.ckeditor.com/4.13.1/basic/ckeditor.js"></script>
<script src="../action/opini_edit.js" type="text/javascript"></script>

<style type="text/css">
  #judul{
    text-transform: uppercase;
  }
  ::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
  }
  :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
    text-transform: none;
  }
  ::-moz-placeholder { /* Mozilla Firefox 19+ */
    text-transform: none;
  }
  :-ms-input-placeholder { /* Internet Explorer 10+ */
    text-transform: none;
  }
  ::placeholder { /* Recent browsers */
    text-transform: none;
  }
</style>