<?php include "../template/header.php"; ?>

<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-0 pd-xl-x-0" style="max-width: none">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Opini</li>
          </ol>
          <h4 class="mg-b-0 tx-spacing--1">OPINI</h4>
          <small></small>
        </nav>
      </div>
      <div class="d-none d-md-block">
        <a href="opini_new" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="plus-circle" class="wd-10 mg-r-5"></i> Tambah Opini</a>
        <button class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5" onclick="get_data()"><i data-feather="refresh-cw" class="wd-10 mg-r-5"></i> Refresh</button>
      </div>
    </div>

    <div class="row row-xs mg-t-10">
      <div class="col-lg-12 col-xl-12 mg-t-12">
        <div class="card">
          <div class="card-body">
            <table id="main-table" class="table" style="width: 100%">
              <thead>
                <th style="text-align: center;">NO</th>
                <th>JUDUL</th>
                <th>GAMBAR</th>
                <th>VIDEO</th>
                <th style="text-align: center;">ACT</th>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/opini.js" type="text/javascript"></script>

<style type="text/css">
  select[name="main-table_length"] {
    border: 1px solid rgb(226, 229, 237);
    padding: 7px 10px;
    border-radius: 5px; 
  }

  .select_xs{
    width: 60px;
  }
</style>