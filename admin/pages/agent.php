<?php include "../template/header.php"; ?>

<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-0 pd-xl-x-0" style="max-width: none">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="home">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="javascript:;">Master</a></li>
            <li class="breadcrumb-item active" aria-current="page">Agent</li>
          </ol>
          <h4 class="mg-b-0 tx-spacing--1">AGENT</h4>
          <small></small>
        </nav>
      </div>
      <div class="" style="display: inline-flex;">
        <select class="custom-select" id="id_status" name="id_status">
          <option value="99">Semua</option>
          <option value="3">Pending</option>
          <option value="0">Terdaftar</option>
          <option value="1">Disetujui</option>
          <option value="2">Ditolak</option>
        </select>
        <button class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5" onclick="get_data()"><i data-feather="refresh-cw" class="wd-10 mg-r-5"></i></button>
      </div>
    </div>

    <div class="row row-xs mg-t-10">
      <div class="col-lg-12 col-xl-12 mg-t-12">
        <div class="card">
          <div class="card-body">
            <table id="main-table" class="table" style="width: 100%">
              <thead style="font-size: 11px">
                <th style="text-align: center;">NO</th>
                <th>PERUSAHAAN</th>
                <th>ALAMAT</th>
                <th>KOTA</th>
                <th>PIMPINAN</th>
                <th>WHATSAPP</th>
                <th style="text-align: center;">REGISTRASI</th>
                <th style="text-align: center;">DISETUJUI</th>
                <th style="text-align: center;">ACT</th>
              </thead>
              <tbody style="font-size: 11px">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document" style="width: 500px;max-width: 500px">
    <div class="modal-content tx-14">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel5">Agent</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table" style="width: 100%;margin-bottom: 0">
          <tbody id="tb_data">
          </tbody>
        </table>
      </div>
      <div class="modal-footer" id="modal_footer">
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document" style="width: 600px;max-width: 600px">
    <div class="modal-content tx-14">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel5">Agent</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_master" data-parsley-validate autocomplete="off">
          <input type="hidden" name="act" id="act" value="edit">
          <input type="hidden" name="id" id="id" value="">
          <div class="form-group row">
            <label for="nama_perusahaan" class="col-sm-3 col-form-label">Nama Perusahaan</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan" />
            </div>
          </div>
          <div class="form-group row">
            <label for="nama_pt" class="col-sm-3 col-form-label">Nama PT</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nama_pt" name="nama_pt" placeholder="Nama PT" />
            </div>
          </div>
          <div class="form-group row">
            <label for="no_anggota" class="col-sm-3 col-form-label">Nomor Anggota</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="no_anggota" name="no_anggota" placeholder="Nomor Anggota" />
            </div>
          </div>
          <div class="form-group row">
            <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" />
            </div>
          </div>
          <div class="form-group row">
            <label for="alamat_2" class="col-sm-3 col-form-label">Alamat 2</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="alamat_2" name="alamat_2" placeholder="Alamat 2" />
            </div>
          </div>
          <div class="form-group row">
            <label for="id_provinsi" class="col-sm-3 col-form-label">Provinsi</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="provinsi" name="provinsi" placeholder="Provinsi" readonly="readonly" />
              <input type="hidden" class="form-control" id="id_provinsi" name="id_provinsi" readonly="readonly" />
            </div>
          </div>
          <div class="form-group row">
            <label for="id_kota" class="col-sm-3 col-form-label">Kota</label>
            <div class="col-sm-9">
              <select class="custom-select" id="id_kota" name="id_kota"></select>
            </div>
          </div>
          <div class="form-group row">
            <label for="telp" class="col-sm-3 col-form-label">No Telp</label>
            <div class="input-group col-sm-9">
              <div class="input-group-prepend">
                <span class="input-group-text">62</span>
              </div>
              <input type="text" class="form-control" id="telp" name="telp" placeholder="No Telp" />
            </div>
          </div>
          <div class="form-group row">
            <label for="email" class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" id="email" name="email" placeholder="Email" />
            </div>
          </div>
          <div class="form-group row">
            <label for="nama_pimpinan" class="col-sm-3 col-form-label">Nama Pimpinan</label>
            <div class="input-group col-sm-9">
              <div class="input-group-prepend">
                <select class="custom-select" id="gelar_pimpinan" name="gelar_pimpinan">
                  <option value="Bapak">Bapak</option>
                  <option value="Ibu">Ibu</option>
                </select>
              </div>
              <input type="text" class="form-control" id="nama_pimpinan" name="nama_pimpinan" placeholder="Nama Pimpinan" />
            </div>
          </div>
          <div class="form-group row">
            <label for="email_pimpinan" class="col-sm-3 col-form-label">Email Pimpinan</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" id="email_pimpinan" name="email_pimpinan" placeholder="Email Pimpinan" />
            </div>
          </div>
          <div class="form-group row">
            <label for="no_wa" class="col-sm-3 col-form-label">Whatsapp</label>
            <div class="input-group col-sm-9">
              <div class="input-group-prepend">
                <span class="input-group-text">62</span>
              </div>
              <input type="text" class="form-control" id="no_wa" name="no_wa" placeholder="Whatsapp">
            </div>
          </div>
          <div class="form-group row">
            <label for="img" class="col-sm-3 col-form-label">Kartu Nama</label>
            <div class="col-sm-4">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="fileThumbnail" id="fileThumbnail">
                <label class="custom-file-label" for="img">Choose file</label>
              </div>
              <input type="hidden" name="file_kartu_nama" id="file_kartu_nama">
            </div>
            <div class="col-sm-5">
              <div id="txt_img" class="mg-t-10"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="id_tampil" class="col-sm-3 col-form-label">Tampilkan di halaman depan</label>
            <div class="col-sm-9">
              <select class="custom-select" id="id_tampil" name="id_tampil">
                <option value="0">Tampilkan</option>
                <option value="1">Sembunyikan</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Batal</button>
        <button type="button" onclick="save()" class="btn btn-primary tx-13">Simpan</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_tampil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content tx-14">
      <div class="modal-body">
        <p class="mg-b-0">Apakah anda ingin menampilkan agent ini di halaman depan?</p>
      </div>
      <div class="modal-footer" id="modal_footer2">
      </div>
    </div>
  </div>
</div>

<?php include "../template/footer.php"; ?>
<script src="../action/agent.js" type="text/javascript"></script>

<style type="text/css">
  select[name="main-table_length"] {
    border: 1px solid rgb(226, 229, 237);
    padding: 7px 10px;
    border-radius: 5px; 
  }

  .select_xs{
    width: 60px;
  }
</style>