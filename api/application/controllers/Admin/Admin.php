<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/Admin_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function check_post(){
        $data = $this->mainmodul->check(strtolower($this->input->post('check')), $this->input->post('type'));
        if (isset($data['error'])) {
            $this->response([
                'code'      => 500,
                'status'    => 'Internal Server Error',
                'info'      => $data['error'],
            ], 200);
        }
        
        $this->response([
            'code'      => 200,
            'status'    => 'Success',
            'data'      => $data,
            'total'     => count($data) > 0 ? count($data) : 0,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'nama_admin'        => strtoupper($this->input->post('nama_admin', true)),
            'jabatan'           => strtoupper($this->input->post('jabatan', true)),
            'username_admin'    => strtolower($this->input->post('username_admin', true)),
            'email_admin'       => $this->input->post('email_admin', true),
            'wa_admin'          => $this->input->post('wa_admin', true),
            'id_status'         => 0,
        );

        if($act == 'add'){
            $data['password_admin']     = md5(md5(123456));
            $sql                        = $this->mainmodul->add($data);
        }else{
            if(!empty($this->input->post('resetPassword'))){
                $data['password_admin'] = md5(md5(123456));
            }
            $sql                        = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_post(){
        $sql    = true;

        $data   = array(
            'id_status'  => 1,
        );

        $sql    = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }
}

