<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/Dashboard_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data['unsaved']    = $this->mainmodul->agent_unsaved();
        $data['pending']    = $this->mainmodul->agent_pending();
        $data['disetujui']  = $this->mainmodul->agent_disetujui();
        $data['ditolak']    = $this->mainmodul->agent_ditolak();
        $data['agent']      = $this->mainmodul->list_agent();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function menu_post(){
        $data['tampil']     = $this->mainmodul->agent_tampil();
        $data['jumlah']     = $this->mainmodul->agent_jumlah();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function tampil_post(){
        $sql    = true;

        $data   = array(
            'id_tampil'  => $this->input->post('id_tampil'),
        );

        $sql    = $this->mainmodul->tampil($data, 1);

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function tampil_agent_post(){
        $sql    = true;

        $data   = array(
            'id_tampil'  => $this->input->post('id_tampil'),
        );

        $sql    = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }
}

