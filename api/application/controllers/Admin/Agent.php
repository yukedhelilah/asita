<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/Agent_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data($this->input->post('id_status'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => (!empty($data) ? count($data) : 0),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function masterCity_get(){
        $get = $this->mainmodul->masterCity();
        
        $data = [
            'status'    => (!empty($get) ? true : false),
            'data'      => $get,
        ];

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function uploadImg_post(){
        $config['upload_path']      = '../files/'.$this->input->post('type');
        $config['allowed_types']    = '*';//'gif|jpg|jpeg|png|pdf';
        $config['max_size']         = 1024 * 8;

        $new_name = time();
        $config['file_name'] = $new_name;

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('img')) {
            $error = array('error' => $this->upload->display_errors());
            $data = [
                'status' => false,
                'message' => $error['error'],
            ];
        } else {
            $arr_image = array('upload_data' => $this->upload->data());
            $data = [
                'status'    => true,
                'url'       => $arr_image['upload_data']['file_name'],
                'folder'    => $this->input->post('type'),
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'nama_perusahaan'   => strtoupper($this->input->post('nama_perusahaan', true)),
            'nama_pt'           => strtoupper($this->input->post('nama_pt', true)),
            'no_anggota'        => $this->input->post('no_anggota', true),
            'alamat'            => $this->input->post('alamat', true),
            'alamat_2'          => $this->input->post('alamat_2', true),
            'kodepos'           => $this->input->post('kodepos', true),
            'id_provinsi'       => $this->input->post('id_provinsi', true),
            'id_kota'           => $this->input->post('id_kota', true),
            'telp'              => $this->input->post('telp', true),
            'email'             => $this->input->post('email', true),
            'gelar_pimpinan'    => $this->input->post('gelar_pimpinan', true),
            'nama_pimpinan'     => $this->input->post('nama_pimpinan', true),
            'email_pimpinan'    => $this->input->post('email_pimpinan', true),
            'no_wa'             => $this->input->post('no_wa', true),
            'file_kartu_nama'   => $this->input->post('file_kartu_nama', true),
            'id_tampil'         => $this->input->post('id_tampil', true),
        );

        if($act == 'add'){
            $sql                        = $this->mainmodul->add($data);
        }else{
            $sql                        = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }

    public function approve_post(){
        $sql    = true;

        $data   = array(
            'id_status'     => $this->input->post('id_status'),
            'id_admin'      => $this->input->post('id_admin'),
            'tgl_disetujui' => date('Y-m-d H:i:s'),
        );

        $sql    = $this->mainmodul->edit($data, $this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
            ], 200);
        }
    }
}

