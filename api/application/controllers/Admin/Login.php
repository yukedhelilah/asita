<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();   
        $this->load->model('Admin/Login_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }
    
    public function login_post(){
        $required = [];
        if(empty($this->input->post('username'))) {$required[] = 'Username';}
        if(empty($this->input->post('password'))) {$required[] = 'Password';}

        if(count($required)>0){
            $error = [
                'reason'    => 'required',
                'message'   => join(", ",$required).' is required',
            ];
            $this->response([
                'code'      => 401,
                'message'   => 'Unauthorized',
                'errors'    => $error,
            ], 200);
        }

        $data = $this->mainmodul->login($this->input->post('username'), md5(md5($this->input->post('password'))));

        if (isset($data['error'])) {
            $error = [
               'message'   => $data['error']
            ];
            $this->response([
                'code'      => 500,
                'message'   => 'Internal Server Error',
                'errors'    => $error,
            ], 200);
        }else if($data == NULL){
            $error = [
               'message'   => 'Your account is not registered'
            ];
            $this->response([
                'code'      => 500,
                'message'   => 'Alert',
                'errors'    => $error,
            ], 200);
        }else{
            $this->response([
                'code'      => 200,
                'message'   => 'Success',
                'data'      => $data,
            ], 200);     
        }
    }
    
    public function forgot_password_post(){
        $data   = $this->mainmodul->forgot_password($this->input->post('username'));

        if ($data != null) {
            $auth   = $this->randomNumber(6);
            $code   = $this->mainmodul->input_auth($auth, $data['id']);

            // $this->sendMail($data['nama_admin'],$data['email_admin'],$code['auth']);

            $this->response([
                'code'      => 200,
                'message'   => 'Success',
                'data'      => $data,
                'auth'      => base64_encode($code['auth']),
            ], 200);                
        } else {
            $this->response([
                'code'      => 500,
                'message'   => 'Failed',
                'data'      => $data,
            ], 200);     
        }
    }

    function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    function sendMail($name, $email, $auth){
        $to = $email;
        $subject = "RESET ASITA ADMIN ACCOUNT";

        $message = "
        <html>
        <head>
            <title>Reset Account ".date('d-m-Y H:i:s')."</title>
        </head>
        <body>
            Hi, ".$name.",<br>
            We got a request to reset your ASITA ADMIN account.<br>
            This is your authentication code : ".$auth."
        </body>
        </html>
        ";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // $headers .= 'From: <noreply@gochinatours.com>' . "\r\n";
        $headers .= 'From: itcreatrix@gmail.com' . "\r\n";

        $send = mail($to,$subject,$message,$headers);
        if (!$send) {
            $errorMessage = error_get_last()['message'];
            print_r(error_get_last());
        }
    }

    public function reset_password_post(){
        $data=array( 
            'password_admin'  => md5(md5($this->input->post('password', true))),
        );

        $sql   = $this->mainmodul->reset_password($this->input->post('id'),$data);

        $this->response([
            'message'   => 'Success',
            'data'      => $sql,
        ], 200);                
    }
}

