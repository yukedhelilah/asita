<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Opini extends App_Public {

    public function __construct(){
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        parent::__construct();
        
        $this->load->model('Admin/Opini_model', 'mainmodul');
    }

    public function index_get(){
        $this->response([
            'code' => 403,
            'message' => 'Forbidden',
        ], 200);
    }

    public function get_data_post(){
        $data = $this->mainmodul->get_data();

        $this->response([
            'status'    => (!empty($data) ? true : false),
            // 'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function get_details_post(){
        $data       = $this->mainmodul->get_details($this->input->post('id'));

        $this->response([
            'status'    => (!empty($data) ? true : false),
            'total'     => count($data),
            'data'      => $data,
        ], 200);
    }

    public function save_post(){
        $sql        = true;
        $act        = $this->input->post('act', true);

        $data=array(
            'judul'         => strtoupper($this->input->post('judul', true)),
            'deskripsi'     => $this->input->post('deskripsi', true),
            'gambar'        => $this->input->post('img1', true),
            'video'         => $this->input->post('img2', true),
            'tgl_posting'   => date('Y-m-d H:i:s'),
        );

        if($act == 'add'){
            $data['id_admin']   = $this->input->post('id_admin');
            $sql                = $this->mainmodul->add($data);
        }else{
            $sql            = $this->mainmodul->edit($data, $this->input->post('id', true));
        }

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }

    public function delete_post(){
        $sql    = true;

        $sql    = $this->mainmodul->delete($this->input->post('id'));

        if($sql == true){ 
            $this->response([
                'code'      => 200,
                'total'     => count($sql),
            ], 200);
        } else {  
            $this->response([
                'code'      => 500,
                'total'     => count($sql),
            ], 200);
        }
    }

    public function uploadImg_post(){
        $files      = $_POST['no'] == 1 ? 'files/postimage/' : 'files/postvideo/';
        $uploadpath = $files;
        $status     = "";
        $msg        = "";
        $file_element_name = 'file_img';

        if ($status != "error")
        {
            $config['upload_path']      = $uploadpath;
            $config['allowed_types']    = 'gif|jpg|jpeg|png|mp4';
            $config['max_size']         = 1024 * 8;

            $new_name = $_POST['no'] == 1 ? 'image_'.time() : 'video_'.time();
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
      
            //$this->upload->initialize($config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg.'*'.$_POST['no'];
    }
}

