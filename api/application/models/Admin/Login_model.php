<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{
    function login($username, $password){
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('username_admin', $username);
        $this->db->where('password_admin', $password);
        $this->db->where('id_status', 0);

        $query = $this->db->get();

        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $tokenKey = $row['username_admin'] . '|' . date('Y-m-d H:i:s');
            return [
                'id'                => $row['id'],
                'nama_admin'        => $row['nama_admin'],
                'jabatan'           => $row['jabatan'],
                'username_admin'    => $row['username_admin'],
                'password_admin'    => $row['password_admin'],
                'email_admin'       => $row['email_admin'],
                'token'             => md5($tokenKey),
                'expired'           => date("Y-m-d"),
            ];
        } else {
            return NULL;
        }
        
        return []; 
    }  
    
    function forgot_password($username){
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('id_status',0);
        $this->db->where('username_admin',$username);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function input_auth($auth,$id){
        $object = array(
            'auth'  => $auth,
        );
        
        $this->db->where('id', $id);
        $this->db->where('id_status', 0);
        $this->db->update('admin', $object);

        if($this->db->affected_rows()){
            $this->db->select('auth');
            $this->db->from('admin');
            $this->db->where('id',$id);

            $query = $this->db->get();
            
            if (!$query) {
                return ['error' => $this->db->error()];
            }
            
            if ($query->num_rows()>0) {
                $row = $query->row_array();
                return $row;
            }
        }else{
            return false;
        }
    }

    function reset_password($id,$data){
        $this->db->where('id',$id);
        $this->db->update('admin', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}