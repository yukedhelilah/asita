<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Agent_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function get_data($id_status){
        $this->db->select('a.*, b.nama_admin, md5(a.id) as code, c.nama as kota');
        $this->db->from('agent a');
        $this->db->join('admin b','a.id_admin=b.id','left');
        $this->db->join('ms_kabupaten c','a.id_kota=c.id_kab');
        $this->db->order_by('a.tgl_registrasi', 'desc');

        if ($id_status != 99) {
            $this->db->where('a.id_status', $id_status);
        }

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function get_details($id){
        $this->db->select('a.*, b.nama as provinsi, c.nama as kota');
        $this->db->from('agent a');
        $this->db->join('ms_provinsi b','a.id_provinsi=b.id_prov');
        $this->db->join('ms_kabupaten c','a.id_kota=c.id_kab');
        $this->db->where('id', $id);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function masterCity(){
        $this->db->select('a.*');
        $this->db->from('ms_kabupaten a');
        $this->db->where('a.id_prov', 35);
        $this->db->order_by('a.nama', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function add($data){
        $this->db->insert('agent', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('agent', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('agent');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}