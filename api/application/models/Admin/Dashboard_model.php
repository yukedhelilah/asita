<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{   
    private $status = '200';
    private $error = '';
    private $data = [];
    private $sub_data = [];

    function agent_unsaved(){
        $this->db->select('*');
        $this->db->from('agent');
        $this->db->where('id_status', 3);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        } else {
            return $query->num_rows();
        }
    }

    function agent_pending(){
        $this->db->select('*');
        $this->db->from('agent');
        $this->db->where('id_status', 0);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        } else {
            return $query->num_rows();
        }
    }

    function agent_disetujui(){
        $this->db->select('*');
        $this->db->from('agent');
        $this->db->where('id_status', 1);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        } else {
            return $query->num_rows();
        }
    }

    function agent_ditolak(){
        $this->db->select('*');
        $this->db->from('agent');
        $this->db->where('id_status', 2);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        } else {
            return $query->num_rows();
        }
    }

    function agent_tampil(){
        $this->db->select('*');
        $this->db->from('menu_agent');
        $this->db->where('id', 1);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->row_array();
            return $row['id_tampil'];
        }
    }

    function agent_jumlah(){
        $this->db->select('a.*, b.nama as kota');
        $this->db->from('agent a');
        $this->db->join('ms_kabupaten b','a.id_kota=b.id_kab');
        $this->db->where('a.id_status', 1);
        $this->db->where('a.id_tampil', 0);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function list_agent(){
        $this->db->select('a.*, b.nama as kota');
        $this->db->from('agent a');
        $this->db->join('ms_kabupaten b','a.id_kota=b.id_kab');
        $this->db->where('id_status', 0);

        $query = $this->db->get();
        
        if (!$query) {
            return ['error' => $this->db->error()];
        }
        
        if ($query->num_rows()>0) {
            $row = $query->result();
            return $row;
        }
    }

    function tampil($data, $id){
        $this->db->where('id', $id);
        $this->db->update('menu_agent', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('agent', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('admin');
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }
}